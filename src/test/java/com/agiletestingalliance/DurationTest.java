package com.agiletestingalliance;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import com.agiletestingalliance.Duration;

public class DurationTest {

    @Test
    public void testDur() {
        Duration duration = new Duration();
        String expected = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        assertEquals(expected, duration.dur());
    }

    @Test
    public void testCalculateIntValue() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Duration duration = new Duration();
        duration.calculateIntValue();
        String expectedOutput = "5\n";
        assertEquals(expectedOutput, outContent.toString());
    }
}