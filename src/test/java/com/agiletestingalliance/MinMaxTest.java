package com.agiletestingalliance;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import com.agiletestingalliance.MinMax;

public class MinMaxTest {

    @Test
    public void testCalculateMinMax() {
        MinMax minMax = new MinMax();
        assertEquals(10, minMax.calculateMinMax(10, 5));
        assertEquals(20, minMax.calculateMinMax(10, 20));
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals("test", minMax.bar("test"));
        assertEquals("", minMax.bar(null));
    }
}