package com.agiletestingalliance;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import com.agiletestingalliance.TestClass;

public class TestClassTest {

    @Test
    public void testGstr() {
        TestClass testClass = new TestClass("test");
        assertEquals("test", testClass.gstr());
    }
}